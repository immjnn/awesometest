import React from 'react';
import {
  createNativeStackNavigator,
  NativeStackNavigationProp,
} from '@react-navigation/native-stack';
import Home from '../screens/home/Home';
import MainQuestion from '../screens/question/MainQuestion';
import Pseudocode from '../screens/question/Pseudocode';
import AuthQuestion from '../screens/question/AuthQuestion';

export type AppNavigationParams = {
  Home: undefined;
  MainQuestion: undefined;
  Pseudocode: undefined;
  AuthQuestion: undefined;
};

const Stack = createNativeStackNavigator<AppNavigationParams>();

export const AppNavigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="MainQuestion" component={MainQuestion} />
      <Stack.Screen name="Pseudocode" component={Pseudocode} />
      <Stack.Screen name="AuthQuestion" component={AuthQuestion} />
    </Stack.Navigator>
  );
};

export type AppNavigationProps = NativeStackNavigationProp<AppNavigationParams>;

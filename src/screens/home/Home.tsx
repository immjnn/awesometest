import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import {AppNavigationProps} from '../../navigation/AppNavigation';

const Home = () => {
  const navigation = useNavigation<AppNavigationProps>();
  return (
    <View style={styles.container}>
      <Text>Soal Test</Text>
      <View style={styles.wrapButton}>
        <Button
          title="Soal Utama"
          onPress={() => navigation.navigate('MainQuestion')}
        />
      </View>
      <View style={styles.wrapButton}>
        <Button
          title="Soal Pseudocode"
          onPress={() => navigation.navigate('Pseudocode')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapButton: {
    padding: 10,
    width: 200,
  },
});

export default Home;

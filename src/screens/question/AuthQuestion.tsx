import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState} from 'react';
import {Button, StyleSheet, Text, TextInput, View} from 'react-native';

type UserParams = {
  username: string;
  password: string;
};

const AuthQuestion = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [userData, setUserData] = useState<UserParams | null>();

  useEffect(() => {
    const getUser = async () => {
      const user = await AsyncStorage.getItem('user');
      console.log(user);
      setUserData(user ? JSON.parse(user) : null);
    };
    getUser();
  }, []);

  const onLogin = async () => {
    const data = {username, password};
    await AsyncStorage.setItem('user', JSON.stringify(data));
    setUserData(data);
  };

  const onLogout = async () => {
    await AsyncStorage.removeItem('user');
    setUserData(null);
    setUsername('');
    setPassword('');
  };

  return (
    <View style={styles.container}>
      {userData ? (
        <View style={styles.content}>
          <Text style={styles.txt}>Selamat Datang, {userData.username}!</Text>
          <Button title="Logout" onPress={() => onLogout()} color="red" />
        </View>
      ) : (
        <View style={styles.content}>
          <Text style={styles.txt}>Login</Text>
          <TextInput
            placeholder="Username"
            onChangeText={txt => setUsername(txt)}
            style={styles.txtInput}
          />
          <TextInput
            placeholder="Password"
            onChangeText={txt => setPassword(txt)}
            style={styles.txtInput}
            secureTextEntry
          />
          <Button
            title="Login"
            onPress={() => onLogin()}
            disabled={!username || !password}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: '80%',
  },
  txtInput: {
    borderWidth: 1,
    width: '100%',
    backgroundColor: 'lightgrey',
    borderRadius: 4,
    marginBottom: 10,
    padding: 10,
    borderColor: 'grey',
    height: 40,
  },
  txt: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingBottom: 20,
  },
});

export default AuthQuestion;

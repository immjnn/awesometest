/* eslint-disable react-native/no-inline-styles */
import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {Button, ScrollView, StyleSheet, Text, View} from 'react-native';
import {AppNavigationProps} from '../../navigation/AppNavigation';
import {fetchPosts, PostsData} from './question.datasource';
import RnHash, {CONSTANTS} from 'react-native-hash';

// 1. array object
const users = [
  {name: 'Imam Jinani', phone: '087877887265'},
  {name: 'Adrea Dovi', phone: '087877887265'},
];

const MainQuestion = () => {
  const navigation = useNavigation<AppNavigationProps>();

  const [user, setUser] = useState(users[0].name);
  const [posts, setPosts] = useState<PostsData[]>([]);

  useEffect(() => {
    loadPost();
    generateHash('15072022', 'Imam', 'pria');
  }, []);

  // 3. http request
  const loadPost = async () => {
    const response = await fetchPosts();
    if (response) {
      setPosts(response.slice(0, 10));
    }
  };

  // 5. function untuk hapus item post
  const onDeletePost = (id: number) => {
    const filterPost = posts.filter(a => a.id !== id);
    setPosts(filterPost);
  };

  // 7. hash dengan sha256
  const generateHash = async (dob: string, name: string, gender: string) => {
    const words = dob + name + gender + 'ifabula';
    const wordsHash = await RnHash.hashString(
      words,
      CONSTANTS.HashAlgorithms.sha256,
    );
    console.log(wordsHash);
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {/* 2. Buat label dan update label */}
      <View style={styles.card}>
        <Text>{user}</Text>
        <Button title="Update name" onPress={() => setUser('Updated')} />
      </View>

      {/* 4. Tampilkan response dari Posts */}
      <View style={styles.card}>
        {posts.map(item => (
          <View key={item.id} style={styles.wrapItem}>
            <View style={{width: '80%'}}>
              <Text>{item.title}</Text>
            </View>
            <View style={{width: '20%'}}>
              <Button title="Delete" onPress={() => onDeletePost(item.id)} />
            </View>
          </View>
        ))}
      </View>

      {/* 9. Logic login atau Auth */}
      <View style={styles.card}>
        <Button
          title="Logic Login"
          onPress={() => navigation.navigate('AuthQuestion')}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 10,
  },
  card: {
    padding: 10,
    borderRadius: 6,
    elevation: 6,
    backgroundColor: 'white',
    marginBottom: 10,
  },
  wrapItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 8,
  },
});

export default MainQuestion;

/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {Text, View} from 'react-native';

const Pseudocode = () => {
  useEffect(() => {
    multipleLoop();
    fibonacci(20);
    generatePyramid(2);
    console.log(number2words(2234));
  }, []);

  // 1. deret angka berkelipatan 5
  const multipleLoop = () => {
    for (let i = 50; i < 105; i++) {
      if (i % 5 === 0) {
        console.log(i);
        if (i <= 60) {
          console.log('KURANG');
        } else if (i > 60 && i <= 70) {
          console.log('CUKUP');
        } else if (i > 70 && i <= 80) {
          console.log('BAIK');
        } else if (i > 80) {
          console.log('LUAR BIASA');
        }
      }
    }
  };

  // 2. fibonacci
  const fibonacci = (n: number) => {
    const result = [0, 1];
    for (var i = 2; i < n; i++) {
      result.push(result[i - 2] + result[i - 1]);
    }
    console.log(result);
  };

  // 3. array pyramid
  const generatePyramid = (n: number) => {
    var output = '';
    for (var i = 1; i <= n; i++) {
      for (var j = 1; j <= i; j++) {
        output += '*';
      }
      console.log(output);
      output = '';
    }
  };

  // 4. number2words
  const number2words = (n: number): string | undefined => {
    var indoNumber = [
      '',
      'satu',
      'dua',
      'tiga',
      'empat',
      'lima',
      'enam',
      'tujuh',
      'delapan',
      'sembilan',
      'sepuluh',
      'sebelas',
    ];

    if (n < 12) {
      return indoNumber[n];
    } else if (n < 20) {
      return number2words(n - 10) + ' belas';
    } else if (n < 100) {
      return (
        number2words(Math.floor(n / 10)) + ' puluh ' + number2words(n % 10)
      );
    } else if (n < 200) {
      return 'seratus ' + number2words(n - 100);
    } else if (n < 1000) {
      return (
        number2words(Math.floor(n / 100)) + ' ratus ' + number2words(n % 100)
      );
    } else if (n < 2000) {
      return 'seribu ' + number2words(n - 1000);
    } else if (n < 1000000) {
      return (
        number2words(Math.floor(n / 1000)) + ' ribu ' + number2words(n % 1000)
      );
    } else if (n < 1000000000) {
      return (
        number2words(Math.floor(n / 1000000)) +
        ' juta ' +
        number2words(n % 1000000)
      );
    } else if (n < 1000000000000) {
      return (
        number2words(Math.floor(n / 1000000000)) +
        ' milyar ' +
        number2words(n % 1000000000)
      );
    } else if (n < 1000000000000000) {
      return (
        number2words(Math.floor(n / 1000000000000)) +
        ' trilyun ' +
        number2words(n % 1000000000000)
      );
    }
  };

  return (
    <View>
      <Text>Pseudocode, hasil test di log semua</Text>
    </View>
  );
};

export default Pseudocode;

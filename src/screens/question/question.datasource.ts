import axios from 'axios';

type ResponseAPI<R = any> = Promise<R | null>;

export type PostsData = {
  userId: number;
  id: number;
  title: string;
  body: string;
};

export type PostsResponse = ResponseAPI<PostsData[]>;

const URL = 'http://jsonplaceholder.typicode.com/posts';

export const fetchPosts = async () => {
  const response = await axios.get<PostsResponse>(URL);
  if (response.status !== 200) {
    return null;
  }
  return response.data;
};
